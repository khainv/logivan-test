import React, { Component } from "react";
import { Field } from "redux-form";
import TextField from "@material-ui/core/TextField";
import styled, { css } from "styled-components";

export const INPUT_TEXT = "text";
export const INPUT_NUMBER = "number";

export const validateForm = {
  required: value => (value ? undefined : "Required")
};

const InputStyled = styled.div`
  input {
    height: 32px;
  }
  ${props =>
    props.width &&
    css`
      input {
        max-width: ${props.width} + "%";
      }
    `};
`;

const InputNumber = field => {
  const {
    input,
    meta: { submitFailed, error },
    width,
    disabled,
    label
  } = field;

  return (
    <InputStyled {...width}>
      <TextField
        label={label}
        helperText={submitFailed && error}
        margin="normal"
        error={submitFailed && error ? true : false}
        type="number"
        disabled={disabled}
        {...input}
      />
    </InputStyled>
  );
};

const InputText = field => {
  const {
    input,
    meta: { submitFailed, error },
    width,
    disabled,
    label
  } = field;
  return (
    <InputStyled {...width}>
      <TextField
        label={label}
        helperText={submitFailed && error}
        margin="normal"
        error={submitFailed && error ? true : false}
        type="text"
        disabled={disabled}
        {...input}
      />
    </InputStyled>
  );
};

class InputField extends Component {
  renderInputComponent = field => {
    const { type } = field;

    switch (type) {
      case INPUT_TEXT:
        return <InputText {...field} />;
      case INPUT_NUMBER:
        return <InputNumber {...field} />;
      default:
        return null;
    }
  };
  render() {
    return (
      <Field
        {...this.props}
        name={this.props.name}
        type={this.props.type}
        label={this.props.label}
        options={this.props.options}
        component={this.renderInputComponent}
      />
    );
  }
}

export default InputField;
