import styled from "styled-components";

export const DriverStyled = styled.div`
  margin: auto;
  padding: 20px;
  height: 100vh;
  button {
    margin-right: 16px;
  }
  & > :first-child {
    margin-bottom: 20px;
  }
`;

export const ModalStyled = styled.form`
  position: absolute;
  width: 500px;
  background-color: white;
  padding: 16px;
  outline: none;
  top: 40%;
  left: 50%;
  transform: translate(-50%, -50%);
  button {
    margin-top: 16px;
    margin-right: 16px;
  }
  p {
    color: red;
  }
`;
