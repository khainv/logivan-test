import React, { Component } from "react";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import { ADD_DRIVER } from "./saga/DriverSaga";
import InputField, {
  INPUT_TEXT,
  INPUT_NUMBER,
  validateForm
} from "../util/InputField";
import { ModalStyled } from "./stylesheet/Driver.style";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";

class AddDriverModal extends Component {
  componentDidUpdate(prevProps) {
    if (
      JSON.stringify(prevProps.currentDriver) !==
      JSON.stringify(this.props.currentDriver)
    ) {
      this.handleInitialize();
    }
  }
  handleInitialize() {
    const { initialize, currentDriver } = this.props;
    if (currentDriver) {
      initialize(currentDriver);
    }
  }
  onSubmitForm = values => {
    this.props.addDriver({
      ...values,
      driverId: parseInt(values.driverId, 10)
    });
    this.props.handleClose();
  };
  close = () => {
    const { handleClose, initialize } = this.props;

    initialize({});
    handleClose();
  };
  render() {
    const { handleSubmit, currentDriver } = this.props;
    return (
      <Modal open={this.props.open} onClose={() => this.close()}>
        <ModalStyled onSubmit={handleSubmit(this.onSubmitForm)}>
          <Typography component="h2" variant="h5">
            Add Driver
          </Typography>
          <InputField
            name="driverId"
            type={INPUT_NUMBER}
            label="Driver ID"
            validate={validateForm.required}
            disabled={currentDriver && currentDriver.driverId ? true : false}
          />
          <InputField
            name="name"
            type={INPUT_TEXT}
            label="Driver Name"
            validate={validateForm.required}
          />
          <InputField
            name="address"
            type={INPUT_TEXT}
            label="Address"
            validate={validateForm.required}
          />
          <Button type="submit" variant="contained" color="primary">
            {currentDriver && currentDriver.driverId ? "UPDATE" : "ADD"}
          </Button>
          <Button
            type="button"
            variant="contained"
            color="primary"
            onClick={() => this.close()}
          >
            Cancel
          </Button>
        </ModalStyled>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  ...state.driverReducer
});

const mapDispatchToProps = dispatch => {
  return {
    addDriver: data => dispatch({ type: ADD_DRIVER, data })
  };
};

export default reduxForm({ form: "addDriverForm" })(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AddDriverModal)
);
