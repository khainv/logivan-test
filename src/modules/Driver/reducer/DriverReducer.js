import {
  FETCH_DRIVER_SUCCESS,
  FETCH_DRIVER_FAILED,
  FETCH_DRIVER_REQUEST,
  ADD_DRIVER_SUCCESS,
  ADD_DRIVER_REQUEST,
  ADD_DRIVER_FAILED,
  DELETE_DRIVER_SUCCESS,
  DELETE_DRIVER_FAILED,
  DELETE_DRIVER_REQUEST,
  UPDATE_DRIVER_REQUEST,
  UPDATE_DRIVER_SUCCESS
} from "../saga/DriverSaga";
const initialState = {
  driverList: [],
  isFetching: false,
  isProcessing: false,
  error: null
};

export default function driverReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_DRIVER_REQUEST: {
      return { ...state, isFetching: true };
    }
    case ADD_DRIVER_REQUEST: {
      return { ...state, isProcessing: true };
    }
    case ADD_DRIVER_SUCCESS: {
      const new_driver = [...state.driverList, action.data];
      return { ...state, driverList: new_driver, isProcessing: false };
    }
    case FETCH_DRIVER_SUCCESS: {
      return { ...state, driverList: action.driver, isFetching: false };
    }
    case ADD_DRIVER_FAILED: {
      return { ...state, error: action.error };
    }
    case FETCH_DRIVER_FAILED: {
      return { ...state, isFetching: false };
    }
    case DELETE_DRIVER_REQUEST: {
      return { ...state, error: null };
    }
    case DELETE_DRIVER_SUCCESS: {
      const filtered = state.driverList.filter(d => d.driverId !== action.data);
      return { ...state, driverList: filtered };
    }
    case DELETE_DRIVER_FAILED: {
      return { ...state, error: action.error };
    }
    case UPDATE_DRIVER_REQUEST: {
      return state;
    }
    case UPDATE_DRIVER_SUCCESS: {
      let newDriverList = state.driverList;
      Object.assign(
        newDriverList[
          newDriverList.findIndex(el => el.driverId === action.data.driverId)
        ],
        action.data
      );
      return {
        ...state,
        driverList: newDriverList
      };
    }
    default:
      return state;
  }
}
