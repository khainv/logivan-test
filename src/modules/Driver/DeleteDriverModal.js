import React, { Component } from "react";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import { DELETE_DRIVER } from "./saga/DriverSaga";
import InputField, { INPUT_NUMBER, validateForm } from "../util/InputField";
import { ModalStyled } from "./stylesheet/Driver.style";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";

class DeleleDriverModal extends Component {
  onSubmitForm = values => {
    const id = parseInt(values.driverId);
    this.props.delete(id, () => this.props.handleClose());
  };

  render() {
    const { handleSubmit, error } = this.props;
    return (
      <Modal open={this.props.open} onClose={this.props.handleClose}>
        <ModalStyled onSubmit={handleSubmit(this.onSubmitForm)}>
          <Typography component="h2" variant="h5">
            Delete Driver
          </Typography>
          <p>{error ? error : ""}</p>
          <InputField
            name="driverId"
            type={INPUT_NUMBER}
            label="Driver ID"
            validate={validateForm.required || error}
          />
          <Button type="submit" variant="contained" color="primary">
            Delete
          </Button>
        </ModalStyled>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  ...state.driverReducer
});

const mapDispatchToProps = dispatch => {
  return {
    delete: (id, cb) => dispatch({ type: DELETE_DRIVER, data: id, cb })
  };
};

export default reduxForm({ form: "deleteDriverForm" })(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(DeleleDriverModal)
);
