import { all, takeLatest, select, fork, put, call } from "redux-saga/effects";

export const FETCH_DRIVER = "driver/FETCH_DRIVER";
export const FETCH_DRIVER_REQUEST = "driver/FETCH_DRIVER_REQUEST";
export const FETCH_DRIVER_SUCCESS = "driver/FETCH_DRIVER_SUCCESS";
export const FETCH_DRIVER_FAILED = "driver/FETCH_DRIVER_FAILED";

export const ADD_DRIVER = "driver/ADD_DRIVER";
export const ADD_DRIVER_REQUEST = "driver/ADD_DRIVER_REQUEST";
export const ADD_DRIVER_SUCCESS = "driver/ADD_DRIVER_SUCCESS";
export const ADD_DRIVER_FAILED = "driver/ADD_DRIVER_FAILED";

export const DELETE_DRIVER = "driver/DELETE_DRIVER";
export const DELETE_DRIVER_REQUEST = "driver/DELETE_DRIVER_REQUEST";
export const DELETE_DRIVER_SUCCESS = "driver/DELETE_DRIVER_SUCCESS";
export const DELETE_DRIVER_FAILED = "driver/DELETE_DRIVER_FAILED";

export const UPDATE_DRIVER = "driver/UPDATE_DRIVER";
export const UPDATE_DRIVER_REQUEST = "driver/UPDATE_DRIVER_REQUEST";
export const UPDATE_DRIVER_SUCCESS = "driver/UPDATE_DRIVER_SUCCESS";
export const UPDATE_DRIVER_FAILED = "driver/UPDATE_DRIVER_FAILED";

const dump_driver = [
  {
    driverId: 1,
    name: "Nguyen Van Thanh",
    address: "200 Nguyen Co Thach"
  },
  {
    driverId: 2,
    name: "Nguyen Thanh Nam",
    address: "215 Hoa Binh"
  },
  {
    driverId: 3,
    name: "Tran Khanh Nhan",
    address: "502 Pham The Hien"
  }
];

export const getDriverState = state => state.driverReducer.driverList;

function getDriver() {
  return new Promise(resolve => {
    setTimeout(resolve(dump_driver), 1000);
  });
}

function* addDriver({ data }) {
  try {
    yield put({ type: ADD_DRIVER_REQUEST });
    const driverList = yield select(getDriverState);
    const test = driverList.find(driver => driver.driverId === data.driverId);
    if (test) {
      yield fork(updateDriver, data);
    } else {
      yield put({ type: ADD_DRIVER_SUCCESS, data });
    }
  } catch (error) {
    yield put({ type: ADD_DRIVER_FAILED, error });
  }
}

function* fetchDriver() {
  try {
    const data = yield call(getDriver);
    yield put({ type: FETCH_DRIVER_SUCCESS, driver: data });
  } catch (error) {
    yield put({ type: FETCH_DRIVER_FAILED, error });
  }
}

function* deleteDriver({ data, cb }) {
  try {
    yield put({ type: DELETE_DRIVER_REQUEST });
    const driverList = yield select(getDriverState);
    const test = driverList.find(driver => driver.driverId === data);
    if (test) {
      yield put({ type: DELETE_DRIVER_SUCCESS, data });
      cb();
    } else {
      yield put({
        type: DELETE_DRIVER_FAILED,
        error: `Cannot find driverId: ${data}`
      });
    }
  } catch (error) {
    yield put({ type: DELETE_DRIVER_FAILED, error });
  }
}

function* updateDriver(data) {
  try {
    yield put({ type: UPDATE_DRIVER_REQUEST });
    yield put({ type: UPDATE_DRIVER_SUCCESS, data });
  } catch (error) {
    yield put({ type: UPDATE_DRIVER_FAILED, error });
  }
}

function* watchAddDriver() {
  yield takeLatest(ADD_DRIVER, addDriver);
}

function* watchFetchDriver() {
  yield takeLatest(FETCH_DRIVER, fetchDriver);
}

function* watchDeleteDriver() {
  yield takeLatest(DELETE_DRIVER, deleteDriver);
}

function* watchUpdateDriver() {
  yield takeLatest(UPDATE_DRIVER, updateDriver);
}

export default function* driverFlow() {
  yield all([
    watchFetchDriver(),
    watchAddDriver(),
    watchDeleteDriver(),
    watchUpdateDriver()
  ]);
}
