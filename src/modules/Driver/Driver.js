import React, { Component } from "react";
import { connect } from "react-redux";
import { FETCH_DRIVER } from "./saga/DriverSaga";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import { DriverStyled } from "./stylesheet/Driver.style";
import AddDriverModal from "./AddDriverModal";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import DeleteDriverModal from "./DeleteDriverModal";

class Driver extends Component {
  state = {
    addModalState: false,
    deleteModalState: false,
    currentDriver: null
  };
  componentDidMount() {
    this.props.fetchDriver();
  }
  closeModal = () => {
    this.setState({ addModalState: false, currentDriver: null });
  };

  openModal = () => {
    this.setState({ addModalState: true });
  };

  closeDeleteModal = () => {
    this.setState({ deleteModalState: false });
  };

  openDeleteModal = () => {
    this.setState({ deleteModalState: true });
  };
  handleUpdate = driver => {
    this.setState({ addModalState: true, currentDriver: driver });
  };

  render() {
    const { driverList } = this.props;
    return (
      <DriverStyled>
        <Grid container alignItems="center">
          <Grid item md={8}>
            <Typography component="h2" variant="h5" gutterBottom>
              Driver List
            </Typography>
          </Grid>
          <Grid item md={4}>
            <Button variant="fab" onClick={this.openModal} color="primary">
              <AddIcon />
            </Button>
            <Button
              variant="fab"
              onClick={this.openDeleteModal}
              color="primary"
            >
              <DeleteIcon />
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={16}>
          {driverList &&
            driverList.map(d => (
              <Grid item xs={12} md={4} key={d.driverId}>
                <Card>
                  <CardContent>
                    <Typography variant="h5" component="h2" gutterBottom>
                      {d.name}
                    </Typography>
                    <div>Address: {d.address}</div>
                    <div>Driver ID: {d.driverId}</div>
                  </CardContent>
                  <CardActions>
                    <Button
                      size="small"
                      variant="contained"
                      color="primary"
                      onClick={() => this.handleUpdate(d)}
                    >
                      Update
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
        </Grid>
        <AddDriverModal
          open={this.state.addModalState}
          handleClose={this.closeModal}
          currentDriver={this.state.currentDriver}
        />
        <DeleteDriverModal
          open={this.state.deleteModalState}
          handleClose={this.closeDeleteModal}
        />
      </DriverStyled>
    );
  }
}

const mapStateToProps = state => ({
  ...state.driverReducer
});

const mapDispatchToProps = dispatch => {
  return {
    fetchDriver: () => dispatch({ type: FETCH_DRIVER })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Driver);
