import { all } from "redux-saga/effects";
import driverFlow from "../modules/Driver/saga/DriverSaga";

const IndexSagas = function*() {
  yield all([driverFlow()]);
};

export default IndexSagas;
