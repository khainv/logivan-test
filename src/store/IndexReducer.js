import { combineReducers } from "redux";
import { reducer as form } from "redux-form";
import driverReducer from "../modules/Driver/reducer/DriverReducer";

const IndexReducer = combineReducers({ form, driverReducer });

export default IndexReducer;
